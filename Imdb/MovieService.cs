﻿using System;
using System.Collections.Generic;
using System.Text;
using Imdb.Repository;
using Imdb.Domain;
using System.Linq;

namespace Imdb
{
    public class MovieService
    {

        private MovieRepo _movieRepo= new MovieRepo();
        private ActorRepo _actorRepo = new ActorRepo();
        private ProducerRepo _producerRepo= new ProducerRepo();



        
//...............................................................................................................................
    //Code to List the Movies
        public void ListMovies()
        {
             
            var movie = _movieRepo.Get();
            
            try
            {
                if (movie.Count == 0)
                {
                    Console.WriteLine("***Please add the Movie Details***");
                }
                _movieRepo.Get().ForEach(movie =>
                {
                    Console.WriteLine("Movie Name :" + movie.Name);
                    Console.WriteLine("Release Year :" + movie.YOR);
                    Console.WriteLine("Plot :" + movie.Plot);
                    Console.WriteLine("Actors  :");
                    movie.Actors.ForEach(Actor =>
                    {
                        Console.WriteLine(Actor + " ");
                    }
                    );
                    Console.WriteLine("Producers: ");
                    movie.Producer.ForEach(Producer =>
                    {
                        Console.WriteLine(Producer + " ");
                    });
                    

                });
            }
            catch (ArgumentException exception)
            {
                Console.WriteLine(exception.Message);
                Console.WriteLine();

            }
            
        }
//................................................................................................................................................
//Code to Get details from the user to the List
        public void AddMovie()
        {
           
            string name, plot;
            int yor;
            Console.WriteLine("Enter Movie Name:");
            name = Console.ReadLine();
            Console.WriteLine("Enter Year of Release:");
            yor = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Enter Movie Plot: ");
            plot = Console.ReadLine();
            Console.WriteLine("Choose the Actors ");
            var actorCount = 1;
            var actors = GetActors();
            actors.ForEach(Person =>
            {
                Console.Write(actorCount + "." + Person.ActorName + "  ");
                actorCount++;

            });
            Console.WriteLine();
    
            var actorsValue = Console.ReadLine();
            
            
            Console.WriteLine("Choose the producer");
            var producerCount = 1;
            var producers = GetProducers();
            producers.ForEach(Person =>
            {
                Console.Write(producerCount + "." + Person.ProducerName + " ");
                producerCount++;
            });
            Console.WriteLine();
            var producerValue = Console.ReadLine();
            
            AddMovies(name, yor, plot, actorsValue, producerValue);



        }
//....................................................................................................................................................
//Code To AddMovies to the List
        public void AddMovies(string name, int yor, string plot, string actorsValue,string producerValue)
        {
           
            var actors = GetActors();
            
            var actorsList = actorsValue.Split(",");
            var selectedActors = new List<string>();
            foreach (var val in actorsList)
            {
                var actorVals = int.Parse(val);
                selectedActors.Add(actors.Select(x => x.ActorName).ElementAt(actorVals - 1));
            }
            
            var producers = GetProducers();
           
            var prodVals = Convert.ToInt32(producerValue);
            var selectedProducer = new List<string>();
            selectedProducer.Add(producers.Select(x => x.ProducerName).ElementAt(prodVals - 1));
            var movie = new Movie()
            {
                Name = name,
                YOR = yor,
                Plot = plot,
                Actors = selectedActors,
                Producer = selectedProducer
            };

            _movieRepo.Add(movie);

        }

        public List<Actor> GetActors()
        {
            return _actorRepo.Get();
        }

        public List<Producer> GetProducers()
        {
            return _producerRepo.Get();
        }
//....................................................................................................................................................
//code to delete movie

        public void DeleteMovie(string name)
        {
            
            var movie = _movieRepo.Get();
            var actors = _actorRepo.Get();
            var producers = _producerRepo.Get();

            try
            {
                if (movie.Count == 0)
                {
                    
                    Console.WriteLine("***No Movie in the list please add atleast one movie***");
                }
            }
            catch (ArgumentException exception)
            {
                Console.WriteLine(exception.Message);
                Console.WriteLine();

            }
           
            _movieRepo.Delete(name);

        }
//...................................................................................................................................................
//Code to Add Actor
        public void AddActor(string name, DateTime Dob)
        {
            Actor actor = new Actor();
            actor.ActorName = name;
            actor.DOB = Dob;
            _actorRepo.Add(actor);
        }
//......................................................................................................................................................
//Code to AddProducer
       
        public void AddProducer(string name, DateTime Dob)
        {
            Producer producer = new Producer();
            producer.ProducerName = name;
            producer.DOB = Dob;
            _producerRepo.Add(producer);
        }
//.....................................................................................................................................................
//Code to Delete Movie
        public bool CheckDeleteMovie()
        {
            var flag = true;
            var movie = _movieRepo.Get();
            var actors = _actorRepo.Get();
            var producers = _producerRepo.Get();

            try
            {
                if (movie.Count == 0)
                {
                    flag = false;
                    Console.("***No Movie in the list please add atleast one movie***");
                }
            }
            catch (ArgumentException exception)
            {
                Console.WriteLine(exception.Message);
                Console.WriteLine();

            }
            return flag;
        }
//.....................................................................................................................................................
//Verification to add movie
        public bool CheckAddMovie()
        {
            var flag = true;
            var actors = _actorRepo.Get();
            var producers = _producerRepo.Get();

            try
            {
                if (actors.Count == 0 && producers.Count == 0)
                {
                    flag = false;
                    Console.WriteLine("***Please add atlease one Actor and Producer***");
                }
                else if (producers.Count == 0)
                {
                    flag = false;
                    Console.WriteLine("***Please add the atlease one Producer***");
                }
                else if (actors.Count == 0)
                {
                    flag = false;
                    Console.WriteLine("***Please add the atlease one Actor***");
                }


            }
            catch (ArgumentException exception)
            {
                Console.WriteLine(exception.Message);
                Console.WriteLine();

            }
            return flag;
        }
//....................................................................................................................................................
//Validation to show movies
        public bool ShowMovies()
        {
            var movie = _movieRepo.Get();
            var flag = true;
            try
            {
                if (movie.Count == 0)
                {
                    Console.WriteLine("***Please add the Movie Details***");
                }
            }
            catch (ArgumentException exception)
            {
                Console.WriteLine(exception.Message);
                Console.WriteLine();

            }
            return flag;
        }



    }
}