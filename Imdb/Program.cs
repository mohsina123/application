﻿using System;
using Imdb.Repository;



namespace Imdb
{
    class Program
    {
        

        static void Main(string[] args)
        {
              
        
              MovieService  _libraryservice = new MovieService();
             

             bool flag = true;
             while(flag)
            {
                Console.WriteLine(@"What Do you Want to do ? Select from the below options
                                 1.List Movies
                                 2.Add Movies
                                 3.Add Actor
                                 4.Add Producer
                                 5.Delete Movie
                                 6.Exit");
                int input;
                Console.WriteLine("Enter Your Choice:");
                if (!int.TryParse(Console.ReadLine(), out input));
                {
                    Console.WriteLine("Enter Valid Input");
                }
                switch(input)
                {
                    case 1:
                        try
                        {
                           
                            _libraryservice.ListMovies();
                        }
                        catch (ArgumentException exception)
                        {
                            Console.WriteLine(exception.Message);
                            Console.WriteLine();

                        }
                        break;
                    case 2:
                        try
                        {
                            if (_libraryservice.CheckAddMovie())
                            {
                                _libraryservice.AddMovie();
                            }
                        }
                        catch (ArgumentException exception)
                        {
                            Console.WriteLine(exception.Message);
                            Console.WriteLine();

                        }


                        break;
                    case 3:
                            try
                          {
                            Console.WriteLine("Enter the Actor Name:");
                            string act = Console.ReadLine();
                            Console.WriteLine("Enter the Date of Birth");
                            string dateTime = Console.ReadLine();
                            DateTime date;
                            if (!DateTime.TryParse(dateTime, out date))
                            {
                                Console.WriteLine("Enter the date in Valid format");
                            }
                            _libraryservice.AddActor(act, date);
                         }
                        catch (ArgumentException exception)
                        {
                            Console.WriteLine(exception.Message);
                            Console.WriteLine();

                        }
                        break;
                    case 4:
                        try
                        {
                            Console.WriteLine("Enter the Producer Name:");
                            string pro = Console.ReadLine();
                            Console.WriteLine("Enter the Date of Birth");
                            var dob = DateTime.Parse(Console.ReadLine());
                            _libraryservice.AddProducer(pro, dob);
                        }
                        catch (ArgumentException exception)
                        {
                            Console.WriteLine(exception.Message);
                            Console.WriteLine();

                        }
                        break;
                    case 5: 
                            try
                            {
                               _libraryservice.CheckDeleteMovie();
                               Console.WriteLine("Enter the Movie Name:");
                               string mv = Console.ReadLine();
                             _libraryservice.DeleteMovie(mv);

                        }
                            catch (ArgumentException exception)
                            {
                                Console.WriteLine(exception.Message);
                                Console.WriteLine();

                            }
                           
                           
                           break;
                    case 6: flag = false;
                            break;
                    default: Console.WriteLine("You have Entered an incorrect option");
                        break;
                }

            }
        }
       
    }
}
