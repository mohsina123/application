﻿Feature: IMDB Application
	IMBD application to show a list of movies along with actors and producers who were part of it.

@addmovie
Scenario: Add movie to a empty list
	Given the Moviename is "Ae dil hain Mushkil"
	And the year of MovieReleaseDate is "2016"
	And the MoviePlot is "Explores friendship,love and heartbreak"
	And the indexesOfActor are "1,2,3"
	And the indexOfProducer is "1"
	When we add the movie
	Then the movielist should look like this
		| MovieName           | MovieReleaseDate | MoviePlot                               |
		| Ae dil hain Mushkil | 2016             | Explores friendship,love and heartbreak |
	And the actor list should look like this
		| ActorName                               |
		| Ranbir Kapoor,Anushka Sharma,Fawad Khan |
	And the producer list should look like this
		| ProducerName |
		| Karan Johar  |

@ListMovie
Scenario: List all the movies present in the list
	When the list is fetched
	Then the movielist should look like this
		| MovieName           | MovieRelease Date | MoviePlot                               |
		| Ae dil hain Mushkil | 2016              | Explores friendship,love and heartbreak |
	And the actor list should look like this
		| ActorName                    |
		| Ranbir Kapoor,Anushka Sharma |
	And the producer list should look like this
		| ProducerName |
		| Karan Johar  |

@AddActor
Scenario: Add Actor
	Given the actor name is "Ranbir Kapoor"
	And the actor DOB is "09/09/1992"
	When  addactor to the list
	Then the actors list should look like this
		| ActorName     | DOB        |
		| Ranbir Kapoor | 09/09/1992 |

@AddProducer
Scenario: Add Producer
	Given the Producer name is "Karan Johar"
	And the Producer DOB is "05/05/1972"
	When addproducer to the list
	Then the producers list should look like this
		| ProducerName | DOB        |
		| Karan Johar  | 05/05/1972 |

@DeleteMovie
Scenario: Delete  the movies present in the list
	Given the Moviename is "Locke and Key"
	When  delete  the movie
	Then the movielist should look like this
		| MovieName           | MovieRelease Date | MoviePlot                               |
		| Ae dil hain Mushkil | 2016              | Explores friendship,love and heartbreak |
	And the actor list should look like this
		| ActorName                    |
		| Ranbir Kapoor,Anushka Sharma |
	And the producer list should look like this
		| ProducerName |
		| Karan Johar  |