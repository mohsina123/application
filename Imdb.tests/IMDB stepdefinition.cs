﻿using System;
using System.Linq;
using Imdb.Repository;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;

namespace Imdb.tests
{
    [Binding]
    
    public class IMDBApplicationSteps
    {
        private readonly MovieService _movieservice = new MovieService();
        private string _movieName;
        private string _plot;
        private DateTime _dob;
        
        private string _actorId;
        private string _producerId;
        private string _actorname;
        private string _producername;
        private int _yor;
        private MovieRepo _movieRepo = new MovieRepo();
        private ActorRepo _actorRepo = new ActorRepo();
        [Given(@"the Moviename is ""(.*)""")]
        public void GivenTheMovienameIs(string movieName)
        {
            _movieName = movieName;
        }
        
        [Given(@"the year of MovieReleaseDate is ""(.*)""")]
        public void GivenTheYearOfMovieReleaseDateIs(int yearOfRelease)
        {
            _yor = yearOfRelease;
        }
        
        
        
        [Given(@"the MoviePlot is ""(.*)""")]
        public void GivenTheMoviePlotIs(string plot)
        {
            _plot = plot;
        }

        [Given(@"the indexesOfActor are ""(.*)""")]
        public void GivenTheIndexesOfActorAre(string actorId)
        {
            _actorId = actorId;
        }

        [Given(@"the indexOfProducer is ""(.*)""")]
        public void GivenTheIndexOfProducerIs(string producerId)
        {
            _producerId = producerId;
        }

        [When(@"we add the movie")]
        public void WhenWeAddTheMovie()
        {
            _movieservice.AddMovies(_movieName,_yor,_plot,_actorId,_producerId);
        }


       
        
        [Given(@"the actor name is ""(.*)""")]
        public void GivenTheActorNameIs(string actorName)
        {
            _actorname = actorName;
        }
        
        [Given(@"the actor DOB is ""(.*)""")]
        public void GivenTheActorDOBIs(DateTime dob)
        {
            _dob = dob;
        }
        
        [When(@"addactor to the list")]
        public void WhenAddactorToTheList()
        {
            _movieservice.AddActor(_actorname, _dob);
        }

        [Then(@"the actors list should look like this")]
        public void ThenTheActorsListShouldLookLikeThis(Table table)
        {
            table.CompareToSet(_movieservice.GetActors());
        }
        
        
        [When(@"the movie is added to the list")]
        public void WhenTheMovieIsAddedToTheList()
        {
            _movieservice.ListMovies();
        }
        
        [When(@"the list is fetched")]
        public void WhenTheListIsFetched()
        {
            _movieservice.ListMovies();
        }
        
        [Then(@"the movielist should look like this")]
        public void ThenTheMovielistShouldLookLikeThis(Table table)
        {
            var result = _movieRepo.Get().Select(x => new {
                MovieName=x.Name,
                MovieReleaseDate=x.YOR,
                MoviePlot=x.Plot

            });
            table.CompareToSet(result);
        }
        
        [Then(@"the actor list should look like this")]
        public void ThenTheActorListShouldLookLikeThis(Table table)
        {
            var res = _movieRepo.Get().Select(x => new
            {
                ActorName = string.Join(",",x.Actors.ToList())

            }) ;

            table.CompareToSet(res);

        }
        
        [Then(@"the producer list should look like this")]
        public void ThenTheProducerListShouldLookLikeThis(Table table)
        {
            var result = _movieRepo.Get().Select(x => new
            {
                ProducerName = x.Producer.FirstOrDefault()
            }) ;
            table.CompareToSet(result);
        }

        [BeforeScenario("addmovie")]
        public void AddActor()
        {    
             
            _movieservice.AddActor("Ranbir Kapoor", new DateTime(1982,09,28));
            _movieservice.AddActor("Anushka Sharma", new DateTime(1988, 05, 1));
            _movieservice.AddActor("Fawad Khan", new DateTime(1991,11,28));

        }
        [BeforeScenario("addmovie")]
        public void AddProducer()
        {

            _movieservice.AddProducer("Karan Johar", new DateTime(1972,5,25));
            

        }

        [Given(@"the Producer name is ""(.*)""")]
        public void GivenTheProducerNameIs(string producerName)
        {
            _producername = producerName;
        }

        [Given(@"the Producer DOB is ""(.*)""")]
        public void GivenTheProducerDOBIs(DateTime dob)
        {
            _dob = dob;
        }
        [When(@"addproducer to the list")]
        public void WhenAddproducerToTheList()
        {
            _movieservice.AddProducer(_producername, _dob);
        }

        [Then(@"the producers list should look like this")]
        public void ThenTheProducersListShouldLookLikeThis(Table table)
        {
            table.CompareToSet(_movieservice.GetProducers());
        }
        [When(@"delete  the movie")]
        public void WhenDeleteTheMovie()
        {
            _movieservice.DeleteMovie(_movieName);
        }

        [BeforeScenario("ListMovie")]
        public void ListMovie()
        {
            _movieservice.AddActor("Ranbir Kapoor", DateTime.Parse("10/09/1999"));
            _movieservice.AddActor("Anushka Sharma", DateTime.Parse("10/09/1999"));
           
            _movieservice.AddProducer("Karan Johar", DateTime.Parse("10/09/1999"));

            _movieservice.AddMovies("Ae dil hain Mushkil", 2016, "Explores friendship,love and heartbreak", "1,2", "1");
           
            _movieservice.ShowMovies();

        }
        [BeforeScenario("DeleteMovie")]
        public void DeleteMovies()
        {


            _movieservice.AddActor("Ranbir Kapoor", DateTime.Parse("10/09/1999"));
            _movieservice.AddActor("Anushka Sharma", DateTime.Parse("10/09/1999"));

            _movieservice.AddProducer("Karan Johar", DateTime.Parse("10/09/1999"));

            _movieservice.AddMovies("Locke and Key", 2020, "Kill the Enemy", "1,2", "1");

            _movieservice.AddMovies("Ae dil hain Mushkil", 2016, "Explores friendship,love and heartbreak", "1,2", "1");
            _movieservice.DeleteMovie("Harr");

        }

    }
}
