﻿using System.Linq;
using Imdb.Domain;
using System.Collections.Generic;

namespace Imdb.Repository
{
    public class ProducerRepo
    {
        private static List<Producer> _producerList;
        public ProducerRepo()
        {
            _producerList = new List<Producer>();
        }
        public void Add(Producer  producer)
        {
            _producerList.Add(producer);
        }
        public List<Producer> Get()
        {
            return _producerList;
        }

    }
}
