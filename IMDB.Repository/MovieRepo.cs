﻿using System.Collections.Generic;
using System.Linq;
using Imdb.Domain;

namespace Imdb.Repository
{
    public class MovieRepo
    {
        private static List<Movie> _movies;
        public MovieRepo()
        {
            _movies = new List<Movie>();

        }
        public void Add(Movie movie)
        {
            _movies.Add(movie);
        }
        public List<Movie> Get()
        {
            return _movies.ToList();
        }
        public void Delete(string moviename)
        {
            var now = _movies.SingleOrDefault(m => m.Name == moviename);
            _movies.Remove(now);
        }
    } 
}
