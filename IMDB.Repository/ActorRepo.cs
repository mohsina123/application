﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using Imdb.Domain;

namespace Imdb.Repository
{
    public class ActorRepo
    {
        
        private static List<Actor> _actorList;
        public ActorRepo()
        {
            _actorList = new List<Actor>();
        }
        public void Add(Actor actor)
        {
            _actorList.Add(actor);
        }
        public List<Actor> Get()
        {
            return _actorList.ToList();

        }
    }
}
