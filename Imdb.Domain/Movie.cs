﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Imdb.Domain
{
    public class Movie
    {
        public string  Name{ get; set; }
        public int  YOR { get; set; }
        public string  Plot { get; set; }

        public List<string> Actors { get; set; }
        public List<string> Producer { get; set; }
    }
}
